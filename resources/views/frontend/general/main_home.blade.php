@extends('frontend.general.layouts.master')
@section('main-content')
  <div class="col-lg-6 posts-section">
    <!-- post starts here -->
    <div class="post">
      <!-- info section of post -->
      <div class="info-wrapper">
        <div class="user-avatar">
          <img src="{{ asset('public/assets/images/user-avatar.png') }}" class="post-user-avatar">
        </div>
        <div class="user-info">
          <a href="#">ashwingrg</a>
          <span class="posted-text">posted 6 hours ago on </span>
          <a href="#">funny</a>
        </div>
      </div><!-- .info-wrapper ends here -->
      <div class="clearfix"></div>
      <div class="title-wrapper">
        <a href="#" class="post-title">फोहोरको थुप्रोमा छाडिएको नवजात शिशुलाई स्तनपान गराइ ज्यान बचाएपछि दक्षिण भारतको कर्नाटकमा एक महिला प्रहरीको सबैले प्रशंसा गरेका छन् । ‘मैले त्यो देख्नै सकिनँ,’ उनले भनिन्, ‘मलाई मेरै बच्चा रोइरहेको जस्तो लाग्यो र बच्चालाई दूध चुसाएँ ।’</a>
      </div>
      <div class="link-wrapper">
        <a href="#"><i class="fa fa-external-link"></i> https://www.google.com/</a>
      </div>
      <div class="points-wrapper">
        <span>159 points &nbsp;|&nbsp; 25 comments</span>
      </div>
      <!-- post actions -->
      <div class="post-actions">
        <i class="fa fa-thumbs-o-up active-text-item"></i>
        <i class="fa fa-thumbs-o-down"></i>
        <i class="fa fa-comments-o"></i>
        <i class="fa fa-ellipsis-h"></i>
        <a href="#" class="share-media share-tw pull-right"><i class="fa fa-twitter"></i> Twitter</a>
        <a href="#" class="share-media share-fb pull-right"><i class="fa fa-facebook"></i> Facebook</a>
      </div><!-- .post-actions ends here -->
    </div><!-- .post ends here -->
    <!-- post starts here -->
    <div class="post">
      <!-- info section of post -->
      <div class="info-wrapper">
        <div class="user-avatar">
          <img src="{{ asset('public/assets/images/user-avatar.png') }}" class="post-user-avatar">
        </div>
        <div class="user-info">
          <a href="#">ashwingrg</a>
          <span class="posted-text">posted 6 hours ago on </span>
          <a href="#">funny</a>
        </div>
      </div><!-- .info-wrapper ends here -->
      <div class="clearfix"></div>
      <div class="title-wrapper">
        <a href="#" class="post-title">There is no shame in getting help. There are over 2,000 licensed therapists online now. You're worth it. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad placeat non, eos odit cupiditate, architecto.</a>
      </div>
      <div class="link-wrapper">
        <a href="#"><i class="fa fa-external-link"></i> https://www.google.com/</a>
      </div>
      <div class="points-wrapper">
        <span>159 points &nbsp;|&nbsp; 25 comments</span>
      </div>
      <!-- post actions -->
      <div class="post-actions">
        <i class="fa fa-thumbs-o-up active-text-item"></i>
        <i class="fa fa-thumbs-o-down"></i>
        <i class="fa fa-comments-o"></i>
        <i class="fa fa-ellipsis-h"></i>
        <a href="#" class="share-media share-tw pull-right"><i class="fa fa-twitter"></i> Twitter</a>
        <a href="#" class="share-media share-fb pull-right"><i class="fa fa-facebook"></i> Facebook</a>
      </div><!-- .post-actions ends here -->
    </div><!-- .post ends here -->
    <!-- post starts here -->
    <div class="post">
      <!-- info section of post -->
      <div class="info-wrapper">
        <div class="user-avatar">
          <img src="{{ asset('public/assets/images/user-avatar.png') }}" class="post-user-avatar">
        </div>
        <div class="user-info">
          <a href="#">ashwingrg</a>
          <span class="posted-text">posted 6 hours ago on </span>
          <a href="#">funny</a>
        </div>
      </div><!-- .info-wrapper ends here -->
      <div class="clearfix"></div>
      <div class="title-wrapper">
        <a href="#" class="post-title">There is no shame in getting help. There are over 2,000 licensed therapists online now. You're worth it.</a>
      </div>
      <div class="link-wrapper">
        <a href="#"><i class="fa fa-external-link"></i> https://www.google.com/</a>
      </div>
      <div class="graphic-content">
        <img src="{{ asset('public/assets/images/test-image1.jpg') }}">
      </div>
      <div class="points-wrapper">
        <span>159 points &nbsp;|&nbsp; 25 comments</span>
      </div>
      <!-- post actions -->
      <div class="post-actions">
        <i class="fa fa-thumbs-o-up active-text-item"></i>
        <i class="fa fa-thumbs-o-down"></i>
        <i class="fa fa-comments-o"></i>
        <i class="fa fa-ellipsis-h"></i>
        <a href="#" class="share-media share-tw pull-right"><i class="fa fa-twitter"></i> Twitter</a>
        <a href="#" class="share-media share-fb pull-right"><i class="fa fa-facebook"></i> Facebook</a>
      </div><!-- .post-actions ends here -->
    </div><!-- .post ends here -->
  </div><!-- .posts-section ends -->
  @include('frontend.general.layouts.rightnav')
@stop
