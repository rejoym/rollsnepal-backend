<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
  /**
   * get the landing page
   * @return View [description]
   */
  public function getIndex() {
    return view('frontend.general.main_home');
  }

  /**
   * get request to displaythe login page
   * @return View [description]
   */
  public function getLogin() {
    return view('frontend.general.login');
  }

}
