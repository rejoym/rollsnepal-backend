<?php

namespace App\Http\Controllers\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category as Category;

class PostController extends Controller
{
  /**
   * function to handle get request of create post page
   * @return View
   */
  public function getCreatePost() {
    $data['all_category'] = Category::all();
    return view('frontend.posts.create_post', compact('data'));
  }

  /**
   * function to handle post request of the post page
   * @return [type] [description]
   */
  public function postCreatePost() {

  }
}
