<?php
// use \Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['uses' => 'General\HomeController@getIndex', 'as' => 'get.home' ]);
Route::get('login', ['uses' => 'Auth\LoginController@getLogin', 'as' => 'login' ]);
Route::post('login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'post.login']);
Route::get('signup', ['uses' => 'Auth\RegisterController@getRegister', 'as' => 'get.signup' ]);
Route::post('signup', ['uses' => 'Auth\RegisterController@postRegister', 'as' => 'post.signup' ]);
Route::get('logout', ['uses' => 'Auth\LoginController@getLogout', 'as' => 'get.logout']);
Route::get('about-us', ['uses' => 'About\AboutUsController@getIndex', 'as' => 'get.aboutus' ]);

Route::group(['middleware' => 'auth'], function() {
    Route::get('user/home', ['uses' => 'User\UserController@getIndex', 'as' => 'get.userhome' ]);
    Route::get('post/create', ['uses' => 'Post\PostController@getCreatePost', 'as' => 'get.createpost' ]);
});
